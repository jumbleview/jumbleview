# README #

### Utility to Set Windows Background ###
* This is the project of the program which allows to change Windows desktop background from a command line, batch file, desktop shortcut or task scheduler.
* As argument it should be supplied with path to image file  (jpg or bmp) or directory with  image files.
* In case of directory program randomly picks the image file and keeps track of used files inside persistent storage
(plain text file) so used files will not be taken next time utility is invoked. When all files from the particular 
directory are used storage is cleared up and all files become available for selection again.
* To set the background the program uses Windows  SystemParametersInfo function. File for this function needs to be in BMP format. As far 
as most common choice for image file is JPG Program converts JPG file into BMP with help of FreeImage library. 
* FreeImage library is used here under the GNU GPL (see http://freeimage.sourceforge.net). 
* My part of  code in this project are absolutely free to use, but I feel myself free of any any responsibility for this code as well. 
* The project was built with MS Visual studio Community Edition. The program was tried successfully on Windwos 8.1, Windows 7 Home and Enterprise edition.
* Whenever you have any questions you may write to jumbleview at gmail dot com.
