// This file contains function JVdesktop called from main.

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <string>
#include <conio.h>
#include <ctype.h>

#include <stdio.h>
#include <sys/timeb.h>
#include <time.h>
#include "FreeImage.h"
#include "jv_arguments.h"
#include "jv_image.h"
#include "jv_directory.h"

void ActivateConsole();

void printLicense()
{
	fprintf(stdout, "This software uses the FreeImage.dll open source image library.\nFreeImage is used under the GNU GPL(see http://freeimage.sourceforge.net\n");
}


void printUsage(const char *program, const char* serror)

{
	
	LPTSTR exeName = 0;
	char pathBuf[MAX_PATH];
	GetFullPathName(program, sizeof(pathBuf), pathBuf, &exeName);

	fprintf(stdout, "Executable %s\n", program);
	if (serror && strlen(serror)) {
		fprintf(stdout, "Arguments error: %s\n",serror);
	}
	fprintf(stdout, "Usage:  %s [OPTIONS]  directory_with_image_files_or_image_file\n",exeName);
	fprintf(stdout, "OPTIONS:\n");
	fprintf(stdout, "-s   or -silent  :program works in silent  mode (with no console opened).\n");
	fprintf(stdout, "-p N or -pause N :program pauses for N seconds before changing background.\n");
	printLicense();
	return;
}

int JVdesktop(int argc, char *argp[])
//int _tmain(int argc, _TCHAR* argp[])
{

  JVArguments args(argc, argp);

  if(args.IsDebugMode())   {
    ActivateConsole();
  }
  for(BOOL block=true;block;block=false)   {
    if(!args.IsValid()){
      printUsage(argp[0],args.GetFailureReason());
      break;
	}

	printLicense();
	if (strlen(args.GetImageFileName())==0)	{
		fprintf(stdout,"Working Directory %s\nImage Directory %s\npause %d\n", args.GetWorkingDirectoryName(), args.GetImageDirectoryName(), args.GetDelayBefore());
	} else {
		fprintf(stdout,"Working Directory %s\nImage File Name %s\nImage Name(no Path) %s\npause %d\n",
			args.GetWorkingDirectoryName(), args.GetImageFileName(), args.GetImageNoPathName(),	args.GetDelayBefore());
	}
    int delay = args.GetDelayBefore();
    if(delay)	{
      fprintf(stdout, "Delayed for %d seconds\n",delay);
      Sleep(delay*1000);
    }
    if(args.IsSingleFile()) {
      JVImage ip(args.GetImageFileName(),args.GetImageNoPathName(),args.GetWorkingDirectoryName());
      BOOL result = ip.SetImage(JVImage::BMP_MODE_as_is);
      if(!result) {
        printUsage(argp[0],ip.GetFailureReason());
        break;
      }
    }else  { // directory ...
      JVDirectory dir( args.GetImageDirectoryName(),args.GetWorkingDirectoryName());
      const char* candidateName = dir.GetCandidateName();
      if(!candidateName){
        printUsage(argp[0],dir.GetFailureReason());
        break;
      }
      JVImage ip(candidateName,dir.GetCandidateNoPathName(),args.GetWorkingDirectoryName());
      BOOL result = ip.SetImage(JVImage::BMP_MODE_copy);
      if(!result) {
        printUsage(argp[0],ip.GetFailureReason());
        break;
      }
  // update file with used names
      dir.UpdateAndReport();
    }
    break;
  }  // for ;;
  if(args.IsDebugMode())   {
    printf("Hit any key to exit...");
    _getch();
  }
  return(0);
}
