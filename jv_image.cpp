#include <windows.h>
#include <algorithm>
#include "FreeImage.h"
#include "jv_image.h"

const char* CURRENT_BMP_FILE = "jv_current.bmp";


JVImage::JVImage(const char* imageName, const char* imageNameNoPath, const char* workDir)
{
  m_ImageName = imageName;
  m_ImageNameNoPath = imageNameNoPath;
  m_WorkDir = workDir;
}

JVImage::ImageType JVImage::GetImageType()
{
	ImageType result = ImageUnknown;
	std::string extension;
	size_t i = m_ImageNameNoPath.find_last_of('.');
	if (i != std::string::npos) {
		extension = m_ImageNameNoPath.substr(i);
	}
	std::transform(extension.begin(),extension.end(),extension.begin(),::tolower); 

	if(extension == ".bmp") {
		result = ImageBMP;
	}else if(extension ==".jpg") {
		result = ImageJPG;
	} else {
		m_FailureReason = "Unsupported Image Type \"";
		m_FailureReason+=extension;
		m_FailureReason+="\"";
	}
	return result;
}

BOOL  JVImage::SetWallPaper(const char* fileName)
{
  char* casted = const_cast<char*> (fileName);
  BOOL result = SystemParametersInfo(
		  SPI_SETDESKWALLPAPER,
		  0,
		  casted,
		  0);
  if(!result) {
    m_FailureReason = "Can not set background file \"";
    m_FailureReason += fileName; m_FailureReason += "\"";
  }
  return result;
}

const char* JVImage::GetResultName()
{
  return CURRENT_BMP_FILE;
}
BOOL JVImage::CopyFile()
{
  BOOL result = false;
  m_ImageNameCurrent = m_WorkDir; m_ImageNameCurrent +=GetResultName();

  FILE* sourceFile = fopen(m_ImageName.c_str(),"rb");
  FILE* destinationFile = fopen(m_ImageNameCurrent.c_str(),"wb");
  for(BOOL block=true; block; block = false)  {
    if(sourceFile == 0) {
      m_FailureReason = "Can not read the file: "; m_FailureReason += m_ImageNameCurrent.c_str();
      break;
    }
    if(destinationFile == 0) {
      m_FailureReason = "Can not write the file: "; m_FailureReason += m_ImageNameCurrent.c_str();
      break;
    }
    unsigned char buffer[1024];
    void* pbuf = (void*)buffer; 

    for(size_t rn=1;rn>0;) {
      rn = fread(pbuf,1,sizeof(buffer),sourceFile);
      if(rn > 0) {
        fwrite(pbuf,1,rn,destinationFile);
      }
    }
    result = true;
    break;
  }
  if (destinationFile) {
	  fclose(destinationFile);
  }
  if(sourceFile) fclose(sourceFile);
  return result;
}
BOOL JVImage::ConvertFile()
{
  BOOL result = false;
  m_ImageNameCurrent = m_WorkDir; m_ImageNameCurrent +=GetResultName();

  FreeImage_Initialise(false);
  FIBITMAP *bitmap = FreeImage_Load(FIF_JPEG,m_ImageName.c_str(),JPEG_ACCURATE);
  for(BOOL block=true;block;block=false)
  {
    if(bitmap == 0) {
      m_FailureReason = "Eroor when converting the file: "; m_FailureReason += m_ImageName.c_str();
      break;
    }
    result = FreeImage_Save(FIF_BMP,bitmap,m_ImageNameCurrent.c_str(),BMP_DEFAULT);
    if(!result) {
      m_FailureReason = "Eroor when saving the file: "; m_FailureReason += m_ImageNameCurrent.c_str();
      result;
      break;
    }
  }
  FreeImage_DeInitialise();
  return result;
}

BOOL JVImage::SetImage(BMP_MODE BMP_mode)
{
  BOOL result = false;
  ImageType it = GetImageType();
  for(BOOL block=true;block;block=false)
  {
    if(it == ImageUnknown) {
      break;
    }
    if(it == ImageBMP && BMP_mode == BMP_MODE_as_is) {// source is the BMP file. Set it as background)
      m_ImageNameCurrent = m_ImageName;
      result = true;
      break;
    }
    if(it == ImageBMP && BMP_mode == BMP_MODE_copy) { // source is the BMP file. Copy it to file with dedicated name, then set...
      result = CopyFile();
      break;
    }
    if(it == ImageJPG) { // source is the JPEG file. Should be converted to BMP then set as wall paper. 
      result = ConvertFile();
      break;
    }
  }
  if(result) {
    result = SetWallPaper(m_ImageNameCurrent.c_str()); 
  }
  return result;
}
const char* JVImage::GetFailureReason()
{
  return m_FailureReason.c_str();
}

