#ifndef JV_IMAGE_H
#define JV_IMAGE__H
#include <string>
#include "FreeImage.h"

class JVImage
{
public:
  JVImage(const char* imageName, const char* imageNameNoPath, const char* workDir);
  enum BMP_MODE{
    BMP_MODE_copy,
    BMP_MODE_as_is
  };
  BOOL SetImage(BMP_MODE mode);
  const char* GetFailureReason();

private:
  enum ImageType {
    ImageUnknown,
    ImageBMP,
    ImageJPG
  };
  BOOL  SetWallPaper(const char* file);
  BOOL CopyFile();
  BOOL ConvertFile();
  const char* GetResultName();
  ImageType   GetImageType();
  std::string m_FailureReason;
  std::string m_ImageName;
  std::string m_ImageNameCurrent;
  std::string m_ImageNameNoPath;
  std::string m_WorkDir;
};

#endif