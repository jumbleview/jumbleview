#include <windows.h>
#include <stdio.h>
#include <string>

#include "jv_arguments.h"

JVArguments::JVArguments(int argc, char *argp[])
{
  char pathBuf[MAX_PATH];
  m_IsValid = false;
  m_DebugMode = true;
  int argsIndex = 1;

  m_DelayBefore = 0;
  
  if(argc<2) {
      m_FailureReason = ""; // empty reasin means no arguments
	  return;
   }
	BOOL silent = false;
	for(int i=1; i<(argc-1); i++) { // read options
		std::string arg = argp[i];
		if (arg == "-s" || arg == "-silent" ) {
				silent = true;
		} else if (arg == "-p" || arg == "-pause") {
				i++;
				if (i < (argc-1) )
				{
					m_DelayBefore = atoi(argp[i]);
				} else {
					m_FailureReason = "option -p provided without number value";
					return;
				}
		} else {
				m_FailureReason = arg;
				m_FailureReason += ": unknown option";
				return;
		}
	}
	// last argument is dirtectory or file
    std::string lastArg(argp[argc-1]);
    DWORD attrd = GetFileAttributes(lastArg.c_str()) ;
    if(attrd == INVALID_FILE_ATTRIBUTES) {
   	  m_FailureReason = "Last argument is neither directory nor file name";
      return;
    }
    LPTSTR a = 0;
    GetFullPathName(lastArg.c_str(),sizeof(pathBuf), pathBuf,&a);
	// image directory
    if((attrd & FILE_ATTRIBUTE_DIRECTORY) == 0) { // argument is file name
      m_ImageFileName = pathBuf;
      m_ImageNoPathName = a;
      *a=0;
      m_ImageDirectoryName = pathBuf;
    }else { // argument is directory name
      m_ImageDirectoryName = pathBuf;
    }
    NormalizeDirectoryName(m_ImageDirectoryName);
	// current (working directory
	GetFullPathName(".",sizeof(pathBuf),pathBuf,&a);
    m_WorkingDirectoryName = pathBuf;
    NormalizeDirectoryName(m_WorkingDirectoryName);

    m_IsValid = true;

	if (silent) {
		m_DebugMode = false;
	}
}

BOOL JVArguments::IsValid()
{
  return m_IsValid;
}


BOOL JVArguments::IsSingleFile()
{
  return m_ImageFileName.length() != 0;
}

const char* JVArguments::GetImageDirectoryName()
{
  return m_ImageDirectoryName.c_str();
}

const char* JVArguments::GetWorkingDirectoryName()
{
  return m_WorkingDirectoryName.c_str();
}

const char* JVArguments::GetImageFileName()
{
  return m_ImageFileName.c_str();
}

const char* JVArguments::GetImageNoPathName()
{
  return m_ImageNoPathName.c_str();
}

const char* JVArguments::GetFailureReason()
{
  return m_FailureReason.c_str();
}

void JVArguments::NormalizeDirectoryName(std::string& s)
{
  if(s[s.length()-1]!='\\') {
    s+="\\";
  }
}  