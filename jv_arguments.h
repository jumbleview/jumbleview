#ifndef JV_ARGUMENTS_H
#define JV_ARGUMENTS_H
#include <string>
#include <set>
#include <vector>

class JVArguments
{
public:
  JVArguments(int argc, char *argp[]);
  BOOL IsValid();
  BOOL IsSingleFile();
  BOOL IsConvertOnly();
  BOOL IsDebugMode(){return m_DebugMode;};
  const char* GetImageDirectoryName();
  const char* GetWorkingDirectoryName();
  const char* GetImageFileName();
  const char* GetImageNoPathName();
  const char* GetFailureReason();
  int  GetDelayBefore(){return m_DelayBefore;};
  void NormalizeDirectoryName(std::string& s);
protected:
private:
  BOOL m_IsValid;
  BOOL m_ConvertOnly;
  BOOL m_DebugMode;
  int m_DelayBefore;
  std::string m_ImageDirectoryName;
  std::string m_WorkingDirectoryName;
  std::string m_ImageFileName;
  std::string m_ImageNoPathName;
  std::string m_FailureReason;
 };
#endif
