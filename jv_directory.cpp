#include <windows.h>
#include "jv_directory.h"
#include <sys/timeb.h>
#include <time.h>

extern const char* CURRENT_BMP_FILE;

const char* UsedStorage::getNameList()
{
  m_names.clear();
  for(_UsedStorageItr itr = m_nameStorage.begin(); itr != m_nameStorage.end(); ++itr)
  {
    std::string current = *itr;
    m_names += current;
  }
  return m_names.c_str();
}

JVDirectory::JVDirectory(const char* imageDir, const char* workDir)
{
  m_ImageDir = imageDir;
  m_WorkDir = workDir;
  unsigned long hashnum = GetElfHash(imageDir);
  char buf[17];
  sprintf(buf,"%x",hashnum);
  m_ImageDirHash = buf;
}

const char*  JVDirectory::GetUsedFilesListName()
{
  m_UsedFileListName = m_WorkDir;
  m_UsedFileListName += "jv_used_";
  m_UsedFileListName +=m_ImageDirHash;
  m_UsedFileListName +=".txt";
  return m_UsedFileListName.c_str();
}

const char* JVDirectory::GetReportFileName()
{
  m_ReportFileName = m_WorkDir;
  m_ReportFileName += "jv_report_";
  m_ReportFileName +=".log";

  return m_ReportFileName.c_str();
}

const char* JVDirectory::GetFailureReason()
{
  return m_FailureReason.c_str();
}

const char* JVDirectory::GetCandidateName()
{
  const char* cRet = 0;
  WIN32_FIND_DATA findFileData;

  ImageNameStorage imStorage;

  FILE* fileWithUsedNamesToRead = fopen(GetUsedFilesListName(),"r");

  if(fileWithUsedNamesToRead != NULL)
  { 
    char names[256];
    for(;fgets(names,sizeof(names),fileWithUsedNamesToRead)!= NULL;)
    {
		if (names[0] != '#') {
			m_uStorage.add(names);
		}
    }
    fclose(fileWithUsedNamesToRead);
  }
  std::string ext = "*.jpg";
  for(int rep = 0; rep != 4 ; ++rep)
  {
    std::string pattern = m_ImageDir + ext;
    HANDLE hFind = FindFirstFile(pattern.c_str(),&findFileData);
    if (hFind == INVALID_HANDLE_VALUE){
      if(ext == "*.jpg") {
       ext = "*.bmp";
        continue;
      }
    }

    BOOL isFound = true;
  
    for(;isFound;isFound = FindNextFile(hFind, &findFileData)){
      std::string s = findFileData.cFileName;
      if(strcmp(s.c_str(),CURRENT_BMP_FILE)== 0)  {
        continue;
      }
      s+="\n";
      if(m_uStorage.isAbsent(s.c_str()))	{ 
        imStorage.add(findFileData.cFileName);
      }
    }//for
    FindClose(hFind);
    if(ext == "*.jpg") {
      ext = "*.bmp";
      continue;
    }
    if(imStorage.isEmpty()) {
      m_uStorage.clear();
      ext = "*.jpg"; // read again
    }else {
      break;
    }
  }
  if(imStorage.isEmpty())  {
    m_FailureReason = "Nothing to show";
    return (0);
  }
  struct _timeb tm;
   
  _ftime(&tm); 
  m_millitm = tm.millitm;
  m_ix = m_millitm%imStorage.size();
  m_CandidateNoPathName = imStorage[m_ix];
  m_CandidateName = m_ImageDir + m_CandidateNoPathName;

  return m_CandidateName.c_str();
}

BOOL JVDirectory::UpdateAndReport()
{
  std::string candidate = m_CandidateNoPathName + "\n";
  m_uStorage.add(candidate.c_str());
  const char* usedList = GetUsedFilesListName();
  FILE* fileWithUsedNamesToWrite = fopen(usedList,"w");
  if(fileWithUsedNamesToWrite)  {
	  fputs("#",fileWithUsedNamesToWrite);
	  fputs(m_ImageDir.c_str(),fileWithUsedNamesToWrite);
	  fputs("\n",fileWithUsedNamesToWrite);
	fputs(m_uStorage.getNameList(),fileWithUsedNamesToWrite);  
	fclose(fileWithUsedNamesToWrite);
  }

  const char* reportFileName = GetReportFileName();
  typedef std::vector<std::string> Report;
  Report report;

  FILE* fileToReport = fopen(reportFileName,"r");
  char reportBuffer[256];

  if(fileToReport)  {
    for(;fgets(reportBuffer,sizeof(reportBuffer),fileToReport)!= NULL;)   {
      report.push_back(reportBuffer);
    }
    fclose(fileToReport);
  }

  fileToReport = fopen(reportFileName,"w");

  if(fileToReport)  {
    time_t ltime;
    time( &ltime );
    std::string stm = ctime(&ltime); 
    sprintf(reportBuffer,"%.24s. New background: %s; used files: %d; millitime=%d;  index=%d\n",stm.c_str(),m_CandidateName.c_str(),m_uStorage.size(),m_millitm,m_ix);
    report.insert(report.begin(),reportBuffer);
    int limit = min(report.size(),365);
    for(int ix =0; ix != limit; ++ix)	{
      fputs(report[ix].c_str(),fileToReport);
    }
    fclose(fileToReport);
  }
  return true;
}

const char* JVDirectory::GetCandidateNoPathName()
{
  return m_CandidateNoPathName.c_str();
}
const char* JVDirectory::GetImageDirHash()
{
  return m_ImageDirHash.c_str();
}

unsigned long JVDirectory::GetElfHash(const char* name)
{ // Andrew Binstock, John Rex ... Practical Algorithms for Programmers ...
  unsigned long h=0, g;
  while(*name)  { 
    unsigned char ch = (unsigned char)*name;
    if(ch == '/') // modified to be independent to how directory is present
      ch = '\\';  
    ch = tolower(ch); // the same
    h = (h << 4) + ch;
    ++name;
    if(g=h & 0xF0000000)
      h ^= g >> 24;

    h &= ~g;
  }
  return h;
}

