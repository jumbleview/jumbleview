#ifndef JV_DIRECTORY_H
#define JV_DIRECTORY_H
#include <string>
#include <set>
#include <vector>

class UsedStorage{
public:
  typedef std::set<std::string> _UsedStorage;
  typedef std::set<std::string>::iterator _UsedStorageItr;
  void add(const char* name){m_nameStorage.insert(name);}
  BOOL isAbsent(const char* name) {return m_nameStorage.find(name)== m_nameStorage.end();} 
  void clear(){m_nameStorage.clear();}
  const char* getNameList();
  unsigned int size() {return m_nameStorage.size();}
private:
  _UsedStorage m_nameStorage;
  std::string m_names;
};//UsedStorage

class ImageNameStorage{
public:
  typedef std::vector<std::string> _ImageStorage;
//  typedef std::vector<std::string>::iterator _ImageStorageItr;
  void add(const char* name){m_imageStorage.push_back(name);}
  const char* operator[](unsigned int i){if(i<m_imageStorage.size())return m_imageStorage[i].c_str(); return NULL;}
  BOOL isEmpty() {return m_imageStorage.empty();}
  unsigned int size() {return m_imageStorage.size();}
private:
  _ImageStorage m_imageStorage;

};//ImageNameStorage

class JVDirectory
{
public:
  JVDirectory(const char* imageDir, const char* workDir);
  const char* GetCandidateName();
  const char* GetCandidateNoPathName();
  const char* JVDirectory::GetUsedFilesListName();
  const char* JVDirectory::GetReportFileName();
  const char* GetFailureReason();
  unsigned long GetElfHash(const char* cstr);
  const char* GetImageDirHash();
  BOOL UpdateAndReport();
private:
  std::string m_WorkDir;
  std::string m_ImageDir;
  std::string m_ImageDirHash;
  std::string m_CandidateName;
  std::string m_CandidateNoPathName;
  std::string m_FailureReason;
  std::string m_UsedFileListName;
  std::string m_ReportFileName;
  UsedStorage m_uStorage;
  int m_ix;
  int m_millitm;
};

#endif
